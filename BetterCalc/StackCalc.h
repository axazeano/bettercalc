//
//  StackCalc.h
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "FloatStack.h"

@interface StackCalc : FloatStack

@property(strong, nonatomic) FloatStack *mainStack;
@property(strong, nonatomic) FloatStack *operatorStack;
@property(strong, nonatomic) 

- (NSNumber *) equals;
- (void) push:(id _Nonnull)item;
@end
