//
//  Stack.m
//  BetterCalc
//
//  Created by Владимир Кубанцев on 16.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stack.h"

@interface Stack()
@end

@implementation Stack
- (instancetype)init {
    self = [super init];
    if (self != nil){
            self.stack = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) push:(NSObject *)item {
    [self.stack addObject:item];
}

- (NSObject *) pop {
    if (self.stack.lastObject != nil) {
        id lastObject = self.stack.lastObject;
        [self.stack removeLastObject];
        return lastObject;
    } else {
        @throw([NSException exceptionWithName:@"Stack error" reason:@"Stack is empty" userInfo:nil]);
    }
}

- (id) top {
    return self.stack.lastObject;
}

- (void) clean {
    [self.stack removeAllObjects];
}
@end

