//
//  FloatStack.h
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#ifndef FloatStack_h
#define FloatStack_h
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>


@interface FloatStack : NSObject

@property (nonatomic, strong) NSMutableArray *objects;

- (void)push:(id _Nonnull)item;
- (id _Nonnull)pop;
- (id _Nonnull)top;
- (void)clean;

@end


#endif /* FloatStack_h */
