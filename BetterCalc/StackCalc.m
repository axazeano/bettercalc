//
//  StackCalc.m
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "StackCalc.h"

@implementation StackCalc

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        self.mainStack = [[FloatStack alloc] init];
        self.operatorStack = [[FloatStack alloc] init];
    }
    return self;
}

- (NSNumber*) equals {
    NSNumber *result = [[NSNumber alloc] initWithInt:1];
    return result;
}

//Overide push method
- (void) push:(id _Nonnull)item {
    // If item is number we shoud move it to main stack
    if ([item isKindOfClass:[NSNumber class]]) {
        [self.mainStack push:item];
    // If item is operator:
    } else if ([item isKindOfClass:[NSString class]]) {
        // If item is ")" we should move all operators into main sthack until top != "("
        if ([item isEqualToString:@")"]) {
            NSString *tmp_item = [self.operatorStack pop];
            
            while (![tmp_item isEqualToString:@"("]) {
                [self.mainStack push:tmp_item];
                tmp_item = [self.operatorStack pop];
            }
            // Else we move item to operator stack
        } else {
            [self.operatorStack push:item];
        }
           }
}
@end
