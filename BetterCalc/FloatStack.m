//
//  FloatStack.m
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FloatStack.h"

@interface FloatStack()

@end

@implementation FloatStack

- (instancetype) init {
    // Инициализация
    self = [super init];
    if (self != nil) {
        self.objects = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) push:(id _Nonnull) item {
    [self.objects addObject:item];
}

- (id _Nonnull) pop {
    NSNumber *returned_value = [self.objects lastObject];
    [self.objects removeLastObject];
    return returned_value;
}

- (id _Nonnull) top {
    NSNumber *returned_value = [self.objects lastObject];
    return returned_value;
}

- (void) clean {
    [self.objects removeAllObjects];
}
@end
