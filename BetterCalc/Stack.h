//
//  Stack.h
//  BetterCalc
//
//  Created by Владимир Кубанцев on 16.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#ifndef Stack_h
#define Stack_h

@interface Stack : NSObject
@property (strong, nonatomic) NSMutableArray<NSNumber *> *stack;

-(void)push:(NSNumber *)item;
-(NSNumber *)pop;
-(NSNumber *)top;
-(void)clean;

@end


#endif /* Stack_h */
