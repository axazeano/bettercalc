//
//  StackCalc.m
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "StackCalc.h"

@implementation StackCalc

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        self.mainStack = [[Stack alloc] init];
        self.operatorStack = [[Stack alloc] init];
    }
    return self;
}

+ (int)priority:(NSString *)item {
    if ([item isEqualToString:@"("] | [item isEqualToString:@")"]) {
        return 0;
    } else if ([item isEqualToString:@"+"] | [item isEqualToString:@"-"]) {
        return 1;
    } else if ([item isEqualToString:@"*"] | [item isEqualToString:@"/"]) {
        return 2;
    } else if ([item isEqualToString:@"^"]) {
        return 3;
    }
    return 4;
}

// Custom push. Build stack for calc. Rounded brackets aren't implemented
- (void)push:(NSString *)item {
    if ([[NSScanner scannerWithString:item] scanFloat:NULL]) {
        [self.mainStack push:item];
        
    } else if ([StackCalc priority:item] > [StackCalc priority:[self.operatorStack peek]]) {
        while (self.operatorStack > 0 && [StackCalc priority:item] <= [StackCalc priority:[self.operatorStack peek]]) {
            [self.mainStack push:[self.operatorStack pop]];
        }
        [self.operatorStack push:item];
    } else {
        [self.operatorStack push:item];
    }
    
}

@end
