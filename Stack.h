//
//  Stack.h
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stack : NSObject
@property(strong, nonatomic) NSMutableArray<NSString *> *objects;

- (void)push:(NSString *) item;
- (NSString *)pop;
- (NSString *)peek;
- (void)clean;
- (int)lenght;

@end
