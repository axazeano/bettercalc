//
//  Stack.m
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "Stack.h"

@implementation Stack

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        self.objects = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)push:(NSString *)item {
    [self.objects addObject:item];
}

- (NSString *)pop {
    NSString *result = [self.objects lastObject];
    [self.objects removeLastObject];
    return result;
}

- (NSString *)peek {
    return [self.objects lastObject];
}

-(void)clean {
    [self.objects removeAllObjects];
}

-(int)lenght {
    return [self.objects count];
}

@end
