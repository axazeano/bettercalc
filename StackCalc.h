//
//  StackCalc.h
//  BetterCalc
//
//  Created by Владимир Кубанцев on 21.10.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "Stack.h"

@interface StackCalc : Stack

@property(strong, nonatomic) Stack *mainStack;
@property(strong, nonatomic) Stack *operatorStack;

- (Stack *) convert;
- (void)push:(NSString *) item;
+ (int)priority:(NSString *) item;
- (NSNumber *)equals;

@end
